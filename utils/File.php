<?php
class File {
    private $file; 
    private $fileName;

    public function __construct(string $fileName, array $arrTypes) {

        $this->file = $_FILES[$fileName];
        $this->fileName = $this->file["name"];

        if ($this->file["name"] == "") {
            throw new FileException("No se ha mandado el archivo");
        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {
            switch ($this->file["error"]) {

                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new FileException("El archivo es demasiado grande");

                case UPLOAD_ERR_PARTIAL:
                    throw new FileException("El archivo no se ha subido completamente");

                default:
                    throw new FileException("Error en la subida del archivo");
                    break;
            }            
        }

        if (in_array($this->file["type"], $arrTypes)===false) {
            throw new FileException("El archivo no es válido");
        }            
    }

    public function getFileName() {
        return $this->fileName;
    }

    public function saveUploadFile(string $ruta) {
        if (is_uploaded_file($this->file["tmp_name"])) {

            if (is_file($ruta.$this->file["name"])) {
                $idUnico = time();
                $this->file["name"] = $idUnico."_".$this->file["name"];
                $this->fileName = $this->file['name'];
            }
            if (move_uploaded_file($this->file["tmp_name"],$ruta.$this->file["name"]) == false){
                throw new FileException("El archivo no se ha podido mover al destino especificado");
            }
        }
        else {
            throw new FileException("El archivo no se ha subido mediante formulario");
        }
    }

    public function copyFile($origin,$destiny){
        if (!is_file($origin.$this->file["name"])) {
            throw new FileException("No existe el archivo ".$origin.$this->file["name"]);
        }
        else if (is_file($destiny.$this->file["name"])){
            throw new FileException("El archivo ".$destiny.$this->file["name"]." ya existe");
        }
        else if (!copy($origin.$this->file["name"], $destiny.$this->file["name"])){
            throw new FileException("No se ha podido copiar el archivo");
        }
    }
}
?>