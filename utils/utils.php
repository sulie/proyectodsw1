<?php
    function activePage($page) {
        if($page == $_SERVER['REQUEST_URI']){
            return true;
        }
        else{
            return false;
        }
    }

    function asociadosShow($array) {
        shuffle($array);

        if (count($array) <= 3) {
            for ($i = 0; $i < count($array); $i++) {
                ?>
                <ul class="list-inline">
                    <li><img src="<?= $array[$i]->getLogo()?>" alt="<?= $array[$i]->getDescripcion()?>" title="<?= $array[$i]->getDescripcion()?>"></li>
                    <li><?= $array[$i]->getNombre()?></li>
                </ul>
                <?php
            }
        }

        else {
            for ($i = 0; $i < 3; $i++) {
                ?>
                <ul class="list-inline">
                    <li><img src="<?= $array[$i]->getLogo()?>" alt="<?= $array[$i]->getDescripcion()?>" title="<?= $array[$i]->getDescripcion()?>"></li>
                    <li><?= $array[$i]->getNombre()?></li>
                </ul>
                <?php
            }
        }
    }
?>