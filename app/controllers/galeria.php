<?php
// require_once "core/App.php";
require_once "utils/utils.php";
require_once "utils/File.php";
require_once "exceptions/FileException.php";
require_once "exceptions/QueryException.php";
require_once "exceptions/AppException.php";
require_once "exceptions/NotFoundException.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "entity/ImagenGaleria.php";
require_once "entity/Categoria.php";
require_once "repository/ImagenGaleriaRepository.php";
require_once "repository/CategoriaRepository.php";

$errores = array();
$descripcion = "";
$imagen = "";

try {
    /*
    $config = require_once("app/config.php");
    App::bind("config",$config);
    */
    
    $imagenGaleriaRepository = new ImagenGaleriaRepository();
    $categoriaRepository = new CategoriaRepository(); 
    

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        $categoria = trim(htmlspecialchars($_POST["categoria"]));
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
        $imagen = new File("imagen", $tiposAceptados);
        $mensaje = "Datos enviados";
        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALERIA);
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALERIA, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

        
        $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);
        $imagenGaleriaRepository->save($imagenGaleria);

    }
    $imagenes = $imagenGaleriaRepository->findAll();
    $categorias = $categoriaRepository->findAll();
}

catch (AppException $appException) {
    array_push($errores, $appException->getMessage());
}

catch (FileException $fileException) {
    array_push($errores, $fileException->getMessage());
}

catch (QueryException $queryException) {
    array_push($errores, $queryException->getMessage());
}

catch (NotFoundException $notFoundException) {
    array_push($errores, $notFoundException->getMessage());
}

require __DIR__."/../views/galeria.view.php";