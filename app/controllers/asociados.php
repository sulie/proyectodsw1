<?php
require "utils/utils.php";
require "utils/File.php";
require "exceptions/FileException.php";
require "entity/Asociado.php";

$descripcion = "";
$logo = "";
$nombre = "";
$errores = [];

if ($_SERVER["REQUEST_METHOD"]==="POST") {
    try {
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $logo = new File("logo", $tiposAceptados);

        $mensaje = "Datos enviados";

        $logo->saveUploadFile(Asociado::RUTA_ASOCIADOS);

        $nombre = trim(htmlspecialchars($_POST["nombre"]));
        $ruta = Asociado::RUTA_ASOCIADOS.$logo->getFileName();
        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        $asociado = new Asociado($nombre,$ruta,$descripcion);
        array_push(Asociado::$asociados,$asociado);
    } catch (FileException $fileException) {
        $errores [] = $fileException->getMessage();
    }
    if ($_POST["nombre"] == "") {
        $error_nombre = "Debes rellenar un nombre";
        array_push($errores,$error_nombre);
    }
}

require __DIR__."/../views/asociados.view.php";
?>