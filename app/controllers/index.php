<?php
require_once "database/QueryBuilder.php";
require_once "database/Connection.php";
require_once "utils/utils.php";
require_once "entity/ImagenGaleria.php";
require_once "repository/ImagenGaleriaRepository.php";

/*
for ($i = 1; $i <= 12; $i++) {
    $imagenes[$i-1] = new ImagenGaleria($i.".jpg", "descripcion imagen ".$i, $i*rand(25,100), $i*rand(10,25), $i*rand(1,10));
}
*/


try {
    /*
    $config = require_once("app/config.php");
    App::bind("config",$config);
    */
    
    $imagenGaleriaRepository = new ImagenGaleriaRepository();
    $imagenes = $imagenGaleriaRepository->findAll();
}

catch (QueryException $qexep) {

}

catch (AppException $aexep) {

}

catch (FileException $fexep) {

}

require __DIR__."/../views/index.view.php";
?>