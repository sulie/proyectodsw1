<?php
// $errors = ["First name is required","The email must been filled","Subject cannot been empty","You must fill a message"];
$errors = array();
if($_SERVER['REQUEST_METHOD']==='POST'){
    if(empty($_POST['nombre'])){
        array_push($errors,"First name is required");
    }
    if(empty($_POST['email'])){
        array_push($errors,"The email must been filled");
    }
    else if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)==false){
        array_push($errors,"Please, enter a valid email");
    }
    if(empty($_POST['asunto'])){
        array_push($errors,"Subject cannot been empty");
    }
    if(empty($_POST['mensaje'])){
        array_push($errors,"You must fill a message");
    }
}
require "utils/utils.php";
require __DIR__."/../views/contact.view.php";
?>