<?php 

return [
    "proyectodsw1" => "app/controllers/index.php",
    "proyectodsw1/about" => "app/controllers/about.php",
    "proyectodsw1/asociados" => "app/controllers/asociados.php",
    "proyectodsw1/blog" => "app/controllers/blog.php",
    "proyectodsw1/contact" => "app/controllers/contact.php",
    "proyectodsw1/galeria" => "app/controllers/galeria.php",
    "proyectodsw1/post" => "app/controllers/single_post.php"
]

?>