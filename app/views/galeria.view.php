<?php
include __DIR__ . "/partials/inicio-doc.partial.php";
include __DIR__ . "/partials/nav.partial.php";

?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <?php if (empty($errores)) {
                    ?>
                    <p><?= $mensaje ?></p>
                    <?php
                }

                else {
                    ?>
                <ul>
                    <?php
                    foreach ($errores as $error) {
                    ?>
                    <li><?= $error ?></li>
                    <?php
                    }
                    ?>
                </ul>   
                    <?php
                }
                ?>
            </div>
            <?php endif;?>
    
            <form class="form-horizontal" action="<?=$_SERVER["PHP_SELF"] ?>" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                
                    <div class="col-xs-12">
                        <label class="label-control">Categoría</label>
                        <select class="form-control" name="categoria">
                        <?php foreach ($categorias as $categoria) : ?>
                        <option value='<?= $categoria->getId()?>'><?= $categoria->getNombre();?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= $descripcion ?>"></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
            <table class="table">
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Visualizaciones</th>
                    <th>Likes</th>
                    <th>Descargas</th>
                    <th>Categoria</th>
                </tr>
                <?php
                /*
                try {
                    $con = new QueryBuilder($pdo);
                    $imagenes = $con->findAll('imagenes','ImagenGaleria');
                }
                
                catch (PDOException $PDOException) {
                    $errores[] = $PDOException->getMessage();
                }
                */
                foreach ($imagenes ?? [] as $tupla) {
                ?>
                <tr>
                    <td><?=$tupla->getId()?></td>
                    <td><?=$tupla->getNombre()?></td>
                    <td><?=$tupla->getDescripcion()?></td>
                    <td><?=$tupla->getNumVisualizaciones()?></td>
                    <td><?=$tupla->getNumLikes()?></td>
                    <td><?=$tupla->getNumDownloads()?></td>
                    <td><?= $imagenGaleriaRepository->getCategoria($tupla)->getNombre() ?></td>
                </tr>
                <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.partial.php"; ?>