<?php
include __DIR__ . "/partials/inicio-doc.partial.php";
include __DIR__ . "/partials/nav.partial.php"; 
?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>ASOCIADOS</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <?php if(empty($errores)) : ?>
                <p><?= $mensaje ?></p>
                <?php else : ?>
                <ul>
                    <?php foreach($errores as $error) : ?>
                    <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <form class="form-horizontal" action="<?=$_SERVER["PHP_SELF"] ?>" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Logo</label>
                        <input class="form-control-file" name="logo" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-6">
                        <label class="label-control">Nombre</label>
                        <input type="text" class="form-control" name="nombre" value="<?= $nombre ?>"></textarea>
                    </div>
                
                    <div class="col-xs-6">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion"><?= $descripcion ?></textarea>
                        <br>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.partial.php"; ?>