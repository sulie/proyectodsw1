<?php
if(activePage('/proyectodsw1/index.php')) {
  $index = "active";
}
else {
  $index = "";
}

if(activePage('/proyectodsw1/about.php')) {
  $about = "active";
}
else {
  $about = "";
}

if(activePage('/proyectodsw1/blog.php')) {
  $blog = "active";
}
else {
  $blog = "";
}

if(activePage('/proyectodsw1/contact.php')) {
  $contact = "active";
}
else {
  $contact = "";
}

if(activePage('/proyectodsw1/galeria.php')) {
  $gallery = "active";
}
else {
  $gallery = "";
}

if(activePage('/proyectodsw1/asociados.php')) {
  $partners = "active";
}
else {
  $partners = "";
}
?>
<!-- Navigation Bar -->
<nav class="navbar navbar-fixed-top navbar-default">
     <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a  class="navbar-brand page-scroll" href="#page-top">
              <span>[PHOTO]</span>
            </a>
         </div>
         <div class="collapse navbar-collapse navbar-right" id="menu">
            <ul class="nav navbar-nav">
              <li class='lien <?php echo $index ?>'><a href="/proyectodsw1"><i class="fa fa-home sr-icons"></i> Home</a></li>
              <li class='lien <?php echo $about ?>'><a href="about"><i class="fa fa-bookmark sr-icons"></i> About</a></li>
              <li class='lien <?php echo $blog ?>'><a href="blog"><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
              <li class='lien <?php echo $contact ?>'><a href="contact"><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>
              <li class='lien <?php echo $gallery ?>'><a href="galeria"><i class="fa fa-image sr-icons"></i> Gallery</a></li>
              <li class='<?php echo $partners ?>'><a href="asociados"><i class="fa fa-hand-o-right sr-icons"></i> Partners</a></li>
            </ul>
         </div>
     </div>
   </nav>
<!-- End of Navigation Bar -->