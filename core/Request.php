<?php
class Request {
    // Obtiene el "PATH" de una URL, es decir, lo que va despues del hostname, y luego añade /
    public static function uri(){
        return trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), "/");
    }
}
?>