<?php 
require "core/bootstrap.php"; // Incluye el archivo App, la configuracion y la conexion con la DB
$routes = require "app/routes.php"; // Incluye las rutas

require $routes[Request::uri()]; // Accede a la ruta que use la uri especificada
?>