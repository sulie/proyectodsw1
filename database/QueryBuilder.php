<?php
require_once __DIR__ ."/../core/App.php";
require_once __DIR__ ."/../database/IEntity.php";
abstract class QueryBuilder {

    private $connection;
    private $table;
    private $classEntity;

    public function __construct(string $table, string $classEntity) {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    public function executeQuery(string $sql): array {

        $pdoStatement = $this->connection->prepare($sql);
        if ($pdoStatement->execute()===false)
        throw new QueryException("No se ha podido ejecutar la consulta");
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity); 
    }

    public function findAll() {
        $sql = "SELECT * FROM $this->table";
        return $this->executeQuery($sql);
    }

    public function save(IEntity $entity):void {
        //try {
            $parameters = $entity->toArray(); // Almacena las propiedades del objeto en un array
            $sql = sprintf('insert into %s (%s) values (%s)', // Escribe una consulta preparada con la tabla, columnas y valores
            $this->table,
            implode(', ', array_keys($parameters)),
            ':'.implode(', :', array_keys($parameters))
            );

            // Prepara y ejecuta la consulta
            $statement = $this->connection->prepare($sql);
            $statement->execute($parameters);
        //}
        /*
        catch (PDOException $exception) {
            throw new QueryException("Error al insertar en la BBDD");
        }
        */
    }

    

    public function find (int $id): IEntity {

        $sql = "SELECT * FROM $this->table WHERE id=$id";
        $result = $this->executeQuery($sql);

        if (empty($result))
        throw new NotFoundException("No se ha encontrado el elemento con id $id");


        return $result[0]; 
    }
    /*
    findAll:
    Realiza la función de fetchAll para la tabla especificada por parámetro en la clase espeificada por parámetro

    fetchAll:
    Realiza la función de FETCH_CLASS para todos los registros de la tabla

    FETCH_CLASS:
    Instancia un registro de la tabla pasada por parámetro como objeto de la clase que se ha mandado por parámetro

    FETCH_PROPS_LATE:
    Llama al constructor de la clase antes de que las propiedades sean asignadas desde los valores de la columna en la base de datos
    */
}
